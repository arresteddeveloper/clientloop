/* 
* Name          : Shift_Funded_Loans_Schedule_Job
* Author        : Shift CRM
* Description   : Schedulable class to update Loans in stage 'Listed Loan', when Loans become 100% funded they move to stage 'Pending Closure'.
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 01/22/2015         Desmond     1.0          Initial
*/

global class Shift_Funded_Loans_Schedule_Job implements Schedulable{

	global List<Loan__c> loanList;

	global Shift_Funded_Loans_Schedule_Job() {
		
		loanList = [SELECT Id, RecordTypeId, Listing_Begins__c, Stage__c FROM Loan__c WHERE Stage__c = 'Listed Loan' AND Funded__c = 100 ];

	}

	global void execute (SchedulableContext ctx){

		//Get Pending Closure Record Type for Loan__c
		RecordType rt = [SELECT Id, name, developerName FROM RecordType WHERE DeveloperName = :Shift_Utility.PENDING_CLOSURE_DEVELOPER_NAME AND sObjectType = :Shift_Utility.LOAN_SOBJECT];

		for (Loan__c l : loanList){

			//Calculate days between
			Decimal dayDiff = Decimal.valueOf(((System.now()).getTime() - (l.Listing_Begins__c).getTime())/ (1000*60*60*24));
			dayDiff = dayDiff.setScale(8);

			//Set Fields
			l.RecordTypeId = rt.Id;
			l.Stage__c = Shift_Utility.FUNDED_PENDING_CLOSURE;
			l.Duration_of_Listing__c = dayDiff;
		}

		Database.update(loanList);
	}
}