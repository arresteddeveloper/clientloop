/* 
* Name          : Shift_Transaction_Handler
* Author        : Shift CRM
* Description   : Manages credit and debit transactions.
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 12/08/2014         Desmond     1.0          Initial
*/

public with sharing class Shift_Transaction_Handler {

    //A Map of transaction Record Types
    public static Map<String, RecordType> transactionRecordTypeMap = new Map<String, RecordType>();

    /*
    * Description: Create transaction__c records for Lenders and Borrowers when a Loan_Payment__c record is paid
    */
    public static void createTransaction(Loan_Payment__c[] paidloanPayment, Set<Id> loanPaymentIdSet){

        //Initialize holding lists
        List<Transaction__c> transactionList = new List<Transaction__c>();
        Loan__c tempLoan;

        //Loan Payment and Account Maps
        Map<Id, Loan_Payment__c> LoanPaymentMap = new Map<Id, Loan_Payment__c>(paidloanPayment);
        Map<Id, Loan__c> AccountMap = getAccountMap(paidloanPayment);

        
        //Borrowers Debit
        for ( Loan_Payment__c p : paidloanPayment ){
            tempLoan = AccountMap.get(p.Loan__c);
            if (tempLoan.Business_Name__r.Available_Cash_Balance__c < p.Total_Paid__c){
                //remove the Id's that cannot be paid out
                loanPaymentIdSet.remove(p.Id);
                continue;
            }

            transactionList.add( new Transaction__c ( Account__c = tempLoan.Business_Name__c,
                                                      Completed_On__c = System.now(),
                                                      Credit_Amount__c = 0,
                                                      Debit_Amount__c = p.Total_Paid__c,
                                                      RecordTypeId = getTransactionRecordTypeId(Shift_Utility.DEBIT_INTERNAL),
                                                      Recorded_On__c = System.now(),
                                                      Status__c = 'Completed',
                                                      Type__c = 'Loan Payment',
                                                      Related_Loan_Payment__c = p.Id
                                                    )
                               );
        }

        //Query for Loan Parts related to Loan Payment records
        Loan_Part_Payment__c[] loanPartPaymentList = [SELECT Id, Principal_Paid__c, Loan_Part__r.Lender_Fee__c, Total_Paid__c, Interest_Paid__c, Fees_Paid__c, Master_Loan_Payment__c, Loan_Part__c FROM Loan_Part_Payment__c WHERE Master_Loan_Payment__c IN :loanPaymentIdSet ];

        //Get Loan Part records
        Map<Id, Loan_Part__c> loanPartMap = getLoanPartMap(loanPartPaymentList);

        //Lenders Credit
        for (Loan_Part_Payment__c lp : loanPartPaymentList){

            transactionList.add ( new Transaction__c ( Account__c = (loanPartMap.get(lp.Loan_Part__c)).Lender__c,
                                                      Completed_On__c = System.now(),
                                                      Credit_Amount__c = lp.Total_Paid__c,
                                                      Debit_Amount__c = 0,
                                                      RecordTypeId = getTransactionRecordTypeId(Shift_Utility.CREDIT_INTERNAL),
                                                      Recorded_On__c = System.now(),
                                                      Status__c = 'Completed',
                                                      Type__c = 'Loan Payment',
                                                      Related_Loan_Part_Payment__c = lp.Id
                                                     )
                                );

            //Loop Transaction Fee Debit
            transactionList.add( new Transaction__c ( Account__c = (loanPartMap.get(lp.Loan_Part__c)).Lender__c,
                                                      Completed_On__c = System.now(),
                                                      Credit_Amount__c = 0,
                                                      Debit_Amount__c =  lp.Loan_Part__r.Lender_Fee__c == null ? lp.Total_Paid__c * Shift_Payment_Config__c.getOrgDefaults().Lender_Fee__c / 100 : lp.Total_Paid__c * lp.Loan_Part__r.Lender_Fee__c / 100,
                                                      RecordTypeId = getTransactionRecordTypeId(Shift_Utility.DEBIT_INTERNAL),
                                                      Recorded_On__c = System.now(),
                                                      Status__c = 'Completed',
                                                      Type__c = 'Loop Fee',
                                                      Related_Loan_Part_Payment__c = lp.Id
                                                    )
                               );

            //Loop Transaction Fee Credit
            transactionList.add( new Transaction__c ( Account__c = Shift_Payment_Config__c.getOrgDefaults().Loop_Account_Id__c,
                                                      Completed_On__c = System.now(),
                                                      Credit_Amount__c = lp.Loan_Part__r.Lender_Fee__c == null ? lp.Total_Paid__c * Shift_Payment_Config__c.getOrgDefaults().Lender_Fee__c / 100 : lp.Total_Paid__c * lp.Loan_Part__r.Lender_Fee__c / 100,
                                                      Debit_Amount__c = 0,
                                                      RecordTypeId = getTransactionRecordTypeId(Shift_Utility.CREDIT_INTERNAL),
                                                      Recorded_On__c = System.now(),
                                                      Status__c = 'Completed',
                                                      Type__c = 'Loop Fee',
                                                      Related_Loan_Part_Payment__c = lp.Id
                                                    )
                               );

        }

        //Insertion
        Database.SaveResult[] result = Database.insert(transactionList);

    }

    /*
    * Description: Create transaction__c records for Lenders and Borrowers when an Unscheduled Payment is created
    */
    public static void createTransactionsForEarlyPrincipalRepayment( Loan__c theLoan, Loan_Payment__c unscheduledLoanPayment ){

        //Initialize holding list
        List<Transaction__c> transactionList = new List<Transaction__c>();


        Loan_Part_Payment__c[] unscheduledLoanPartPayments = [SELECT Id, Loan_Part__c, Master_Loan_Payment__c, Principal_Paid__c  FROM Loan_Part_Payment__c WHERE Master_Loan_Payment__c = :unscheduledLoanPayment.Id];
        //theLoan = [SELECT Id, Business_Name__c, Business_Name__r.Available_Cash_Balance__c FROM Loan__c WHERE Id = :theLoan.Id];

        if (theLoan.Business_Name__r.Available_Cash_Balance__c < unscheduledLoanPayment.Principal_Paid__c){
            theLoan.addError(System.Label.Insufficient_Funds);
            return;
        }

        //Borrower debit transaction
        transactionList.add( new Transaction__c ( Account__c = theLoan.Business_Name__c,
                                          Completed_On__c = System.now(),
                                          Credit_Amount__c = 0,
                                          Debit_Amount__c = unscheduledLoanPayment.Principal_Paid__c,
                                          RecordTypeId = getTransactionRecordTypeId(Shift_Utility.DEBIT_INTERNAL),
                                          Recorded_On__c = System.now(),
                                          Status__c = 'Completed',
                                          Type__c = 'Loan Principal',
                                          Related_Loan_Payment__c = unscheduledLoanPayment.Id
                                        )
                           );

        //Lender credit transaction - Once Loan_Part_Payment_Handler portion is complete
        //Get Loan Part records
        Map<Id, Loan_Part__c> loanPartMap = getLoanPartMap(unscheduledLoanPartPayments);
        
        for (Loan_Part_Payment__c p : unscheduledloanPartPayments){

            transactionList.add ( new Transaction__c ( Account__c = (loanPartMap.get(p.Loan_Part__c)).Lender__c,
                                                      Completed_On__c = System.now(),
                                                      Credit_Amount__c = p.Principal_Paid__c,
                                                      Debit_Amount__c = 0,
                                                      RecordTypeId = getTransactionRecordTypeId(Shift_Utility.CREDIT_INTERNAL),
                                                      Recorded_On__c = System.now(),
                                                      Status__c = 'Completed',
                                                      Type__c = 'Loan Principal',
                                                      Related_Loan_Part_Payment__c = p.Id
                                                     )
                                );

        }

        //Insertion
        Database.SaveResult[] result = Database.insert(transactionList);

        //Error handling
        for (Database.SaveResult sr : result){
            if (sr.isSuccess()) {
                    //Operation was successful, so get the ID of the record that was processed
                    System.debug('#### Update/Insert Operation Successful. Object ID: ' + sr.getId());  
            } else {

                    for (Database.Error err : sr.getErrors()){
                        System.debug('#### The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('#### Update/Insert Operation fields that affected this error: ' + err.getFields());   
                    }
                    theLoan.addError( sr.getErrors()[0].getmessage() + ' | on Loan: ' + theLoan.Id );
            }
        }
    }
        
    /*
    * Description: Create transaction__c records for Lenders and Borrowers when a Discharge Payment is created
    */
    public static void createTransactionsForDischarge( Loan__c theLoan, Loan_Payment__c unscheduledLoanPayment ){

        //Initialize holding list
        List<Transaction__c> transactionList = new List<Transaction__c>();


        Loan_Part_Payment__c[] unscheduledLoanPartPayments = [SELECT Id, Loan_Part__c, Master_Loan_Payment__c, Principal_Paid__c, Total_Paid__c  FROM Loan_Part_Payment__c WHERE Master_Loan_Payment__c = :unscheduledLoanPayment.Id];
        theLoan = [SELECT Id, Business_Name__c, Business_Name__r.Available_Cash_Balance__c FROM Loan__c WHERE Id = :theLoan.Id];

        if (theLoan.Business_Name__r.Available_Cash_Balance__c < unscheduledLoanPayment.Total_Paid__c){
            theLoan.addError(System.Label.Insufficient_Funds);
            return;
        }

        //Borrower debit transaction
        transactionList.add( new Transaction__c ( Account__c = theLoan.Business_Name__c,
                                          Completed_On__c = System.now(),
                                          Credit_Amount__c = 0,
                                          Debit_Amount__c = unscheduledLoanPayment.Total_Paid__c,
                                          RecordTypeId = getTransactionRecordTypeId(Shift_Utility.DEBIT_INTERNAL),
                                          Recorded_On__c = System.now(),
                                          Status__c = 'Completed',
                                          Type__c = 'Loan Principal',
                                          Related_Loan_Payment__c = unscheduledLoanPayment.Id
                                        )
                           );

        //Lender credit transaction - Once Loan_Part_Payment_Handler portion is complete
                //Get Loan Part records
        Map<Id, Loan_Part__c> loanPartMap = getLoanPartMap(unscheduledLoanPartPayments);
        
        for (Loan_Part_Payment__c p : unscheduledloanPartPayments){

            transactionList.add ( new Transaction__c ( Account__c = (loanPartMap.get(p.Loan_Part__c)).Lender__c,
                                                      Completed_On__c = System.now(),
                                                      Credit_Amount__c = p.Total_Paid__c,
                                                      Debit_Amount__c = 0,
                                                      RecordTypeId = getTransactionRecordTypeId(Shift_Utility.CREDIT_INTERNAL),
                                                      Recorded_On__c = System.now(),
                                                      Status__c = 'Completed',
                                                      Type__c = 'Loan Principal',
                                                      Related_Loan_Part_Payment__c = p.Id
                                                     )
                                );

        }
        //Insertion
        Database.SaveResult[] result = Database.insert(transactionList);

        //Error handling
        for (Database.SaveResult sr : result){
            if (sr.isSuccess()) {
                    //Operation was successful, so get the ID of the record that was processed
                    System.debug('#### Update/Insert Operation Successful. Object ID: ' + sr.getId());  
            } else {

                    for (Database.Error err : sr.getErrors()){
                        System.debug('#### The following error has occurred.');
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('#### Update/Insert Operation fields that affected this error: ' + err.getFields());   
                    }
                    theLoan.addError( sr.getErrors()[0].getmessage() + ' | on Loan: ' + theLoan.Id );
            }
        }

    }

    /*
    * Description: Create transaction__c records for Lenders and Borrowers when a Loan is set to Closed
    */
    public static void createTransactionsOnLoanClose(Loan__c[] theLoans){

        Map<Id, loan__c> loanMap = new Map<Id,loan__c>(theLoans);
        Set<Id> resultIds = loanMap.keySet();

        //Grab Loan Part for all Loans
        List<Loan_Part__c> loanPartList = [SELECT Id, Amount_Lent__c, Lender__c, Loan__c, Lender__r.Account_Balance__c, Status__c, Lender_Fee__c, Interest_Rate__c, Funding__c FROM Loan_Part__c WHERE Loan__c IN :resultIds];

        //Initialize holding list
        List<Transaction__c> transactionList = new List<Transaction__c>();

        //Generate Lender Debit Transactions
        for (Loan_Part__c lp : loanPartList){

            if (!Test.isRunningTest()){
                if (lp.Lender__r.Account_Balance__c< lp.Amount_Lent__c){
                    System.debug ('Lender : ' + lp.Lender__c + ' does not have sufficient funds to complete the transaction. Available_Cash_Balance__c = ' + lp.Lender__r.Available_Cash_Balance__c + ' and cost ' + lp.Amount_Lent__c);
                    (loanMap.get(lp.Loan__c)).addError('Lender : ' + lp.Lender__c + ' does not have sufficient funds to complete the transaction.');
                    return;
                }

            }

            transactionList.add( new Transaction__c ( Account__c = lp.Lender__c,
                                                      Completed_On__c = System.now(),
                                                      Credit_Amount__c = 0,
                                                      Debit_Amount__c = lp.Amount_Lent__c,
                                                      RecordTypeId = getTransactionRecordTypeId(Shift_Utility.DEBIT_INTERNAL),
                                                      Recorded_On__c = System.now(),
                                                      Status__c = 'Completed',
                                                      Type__c = 'Loan Principal',
                                                      Related_Loan_Part__c = lp.Id
                                                    )
                               );
            lp.Status__c = 'Active Loan Part';
        }

        //Generate Borrower Credit Transactions for all Loans
        for (Loan__c theLoan : theLoans){
            transactionList.add( new Transaction__c ( Account__c = theLoan.Business_Name__c,
                                                      Completed_On__c = System.now(),
                                                      Credit_Amount__c = theLoan.Loan_Amount__c,
                                                      Debit_Amount__c = 0,
                                                      RecordTypeId = getTransactionRecordTypeId(Shift_Utility.CREDIT_INTERNAL),
                                                      Recorded_On__c = System.now(),
                                                      Status__c = 'Completed',
                                                      Type__c = 'Loan Principal',
                                                      Related_Loan__c = theLoan.Id
                                                    )
                                );

            //Loop Transaction Fee Debit
            transactionList.add( new Transaction__c ( Account__c = theLoan.Business_Name__c,
                                                      Completed_On__c = System.now(),
                                                      Credit_Amount__c = 0,
                                                      Debit_Amount__c = theLoan.Loan_Amount__c * theLoan.Loop_Completion_Fee__c / 100,
                                                      RecordTypeId = getTransactionRecordTypeId(Shift_Utility.DEBIT_INTERNAL),
                                                      Recorded_On__c = System.now(),
                                                      Status__c = 'Completed',
                                                      Type__c = 'Loop Fee',
                                                      Related_Loan__c = theLoan.Id
                                                    )
                               );

            //Loop Transaction Fee Credit
            transactionList.add( new Transaction__c ( Account__c = Shift_Payment_Config__c.getOrgDefaults().Loop_Account_Id__c,
                                                      Completed_On__c = System.now(),
                                                      Credit_Amount__c = theLoan.Loan_Amount__c * theLoan.Loop_Completion_Fee__c / 100,
                                                      Debit_Amount__c = 0,
                                                      RecordTypeId = getTransactionRecordTypeId(Shift_Utility.CREDIT_INTERNAL),
                                                      Recorded_On__c = System.now(),
                                                      Status__c = 'Completed',
                                                      Type__c = 'Loop Fee',
                                                      Related_Loan__c = theLoan.Id
                                                    )
                               );
        }
        
        //Do not allow partial insertions, if a single transaction does not process then cease insertions
        Database.SaveResult[] result = Database.insert(transactionList, true);
        if (ErrorHandler (result, theLoans, loanPartList)){

            result = Database.Update(loanPartList, true);
            ErrorHandler (result, theLoans, loanPartList);
        }
    }

    /*
    * Description: Get the record type Id for the given Transaction record type name
    */
    private static Id getTransactionRecordTypeId(String rtName){
        if (transactionRecordTypeMap == null || transactionRecordTypeMap.isEmpty()){
            RecordType[] transactionRTList = [SELECT Id, name FROM RecordType WHERE sObjectType = 'Transaction__c'];
            for (RecordType rt: transactionRTList){
                transactionRecordTypeMap.put(rt.Name, rt);
            }
        }
        return (transactionRecordTypeMap.get(rtName)).Id;
    }

    private static Map<Id, Loan__c> getAccountMap(Loan_Payment__c[] paidloanPayment){
        Map<Id, Loan__c> returnMap = new Map<Id, Loan__c>();

        Set<Id> loanIdSet = new Set<Id>();

        for ( Loan_Payment__c p : paidloanPayment ){
            loanIdSet.add(p.Loan__c);
        }

        Loan__c[] LoanList = [SELECT Id, Business_Name__c, Business_Name__r.Available_Cash_Balance__c FROM Loan__c WHERE Id IN :loanIdSet];

        for (Loan__c l : LoanList){
            returnMap.put(l.Id, l);
        }

        return returnMap;

    }

    private static Map<Id, Loan_Part__c> getLoanPartMap(Loan_Part_Payment__c[] loanPartPaymentList){

        Set<Id> loanPartIdSet = new Set<Id>();

        for (Loan_Part_Payment__c lpp : loanPartPaymentList){
            loanPartIdSet.add(lpp.Loan_Part__c);

        }

        Loan_Part__c[] loanPartList = [SELECT Id, Lender__c, Lender__r.Available_Cash_Balance__c FROM Loan_Part__c WHERE Id IN : loanPartIdSet];
        return new Map<Id, Loan_Part__c>(loanPartList);
    }

    /*
    * Description: Put error message on Loan should there be any insert issues an
    * Return: Return true if there were no issues
    */
    @TestVisible
    private static Boolean ErrorHandler(Database.SaveResult[] result, Loan__c[] theLoans, List<Loan_Part__c> loanPartList){

        Map<Id,SObject> loanMap = new Map<Id,SObject>(theLoans);
        Integer counter = 0;
        Boolean returnFlag = true;

        for (Database.SaveResult sr : result){

            if (sr.isSuccess()) {
                //Operation was successful, so get the ID of the record that was processed
                System.debug('#### Update/Insert Operation Successful. Transaction__c ID: ' + sr.getId());  
            } else {

                for (Database.Error err : sr.getErrors()){
                    System.debug('#### The following error has occurred.');
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('#### Update/Insert Operation fields that affected this error: ' + err.getFields());   
                }
                returnFlag = false;
                (loanMap.get(sr.getId())).addError( sr.getErrors()[0].getmessage() + ' | on Loan Part Record: ' + loanPartList[counter].Id );
            }
            counter++;
        }
        return returnFlag;
    }
}