@isTest
public class Shift_Utility_Test_Factory {
	
	//Set Loop
	public static Account loopAccount;
    
    //Custom Settings
    public static Shift_Payment_Config__c csSettings;

    public static void setupOrg(){

        if (Shift_Utility_Test_Factory.loopAccount == null){
            Shift_Utility_Test_Factory.loopAccount = createAccount();
        }

        if (csSettings == null){
            Shift_Utility_Test_Factory.csSettings = new Shift_Payment_Config__c( Loop_Account_Id__c = Shift_Utility_Test_Factory.loopAccount.Id, grace_period__c = 7, Lender_Fee__c = 1.5);
            insert csSettings;
        }
    }

    public static Account createAccount(){
        Account borrower = new Account(Name='shift test account', status__c = 'Active', Verification__c = 'Verified');
        insert borrower;

        Transaction__c[] moneyList = new List<Transaction__c>();
        RecordType transRT =  [SELECT Id FROM RecordType WHERE DeveloperName = :Shift_Utility.CREDIT_INTERNAL_DEVELOPER_NAME AND SobjectType = :Shift_Utility.TRANSACTION_SOBJECT];

        moneyList.add ( new Transaction__c (  Account__c = borrower.Id,
                                              RecordTypeId = transRT.Id,
                                              Method__c = 'Pre-Authorized Deposit', 
                                              Status__c= 'Completed', 
                                              Completed_On__c = System.now(), 
                                              Type__c = 'Deposit', 
                                              Credit_Amount__c = 8000)
                      );

        insert moneyList;

        return borrower;

    }
	
}