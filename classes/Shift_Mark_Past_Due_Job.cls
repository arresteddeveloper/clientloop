/* 
* Name          : Shift_Mark_Past_Due_Job
* Author        : Shift CRM
* Description   : Schedulable class to update Past Due Payments
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 12/17/2014         Desmond     1.0          Initial
*/


global class Shift_Mark_Past_Due_Job implements Schedulable{

	global list<Loan_Payment__c> paymentList;

	global Shift_Mark_Past_Due_Job() {
		//Query for Loan Payments
		paymentList = [SELECT Id, Payment_Due_in_the_Past__c , Due_Date_in_Past__c FROM Loan_Payment__c where (Payment_Due_in_the_Past__c = true and Due_Date_in_Past__c = false) or (Payment_Due_in_the_Past__c = false and Due_Date_in_Past__c = true)];
	}

	global void execute(SchedulableContext ctx){

		for (Loan_Payment__c lp : paymentList){

			//Set fields
			lp.Due_Date_in_Past__c = lp.Payment_Due_in_the_Past__c;
		}

		Database.update(paymentList, false);
		
	}
}