/* 
* Name          : Shift_Discharge_Loan
* Author        : Shift CRM
* Description   : Class to handle Loan discharges (paying off entire loan)
* Sharing Rules	: With sharing modifier used to run as User
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 01/26/2015         Desmond     1.0          Initial
* 
*/

public with sharing class Shift_Discharge_Loan {

	/*
	* Discharge Loan
	*/ 
	public static void dischargeLoan(Loan__c loan, Decimal principalPaid, Decimal interestPaid, Decimal feesPaid){

		//Query for the appropriate recordtype
		RecordType dischargeRecordType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :Shift_Utility.Discharge_Payment AND SobjectType = :Shift_Utility.LOAN_PAYMENT_SOBJECT];
		System.debug('Discharge Type is : ' + dischargeRecordType.Id + ' DeveloperName: ' + dischargeRecordType.DeveloperName);
		//Create the discharge Loan Payment for insertion
		Loan_Payment__c dischargePayment = new Loan_Payment__c (Loan__c = loan.id, 
									Principal_Owed__c = principalPaid,
									Principal_Paid__c = principalPaid,
									Principal_Scheduled__c = principalPaid,
									Interest_Owed__c = interestPaid,
									Interest_Paid__c = interestPaid,
									Interest_Scheduled__c = interestPaid,
									Fees_Owed__c = feesPaid,
									Fees_Paid__c = feesPaid,
									RecordTypeId = dischargeRecordType.Id,
									Payment_Date__c = Date.today()
									);

		List<Loan_Payment__c> tempList = new List<Loan_Payment__c>();
		tempList.add(dischargePayment);

		//Insert Unscheduled Loan Payment
		//Shift_UnscheduledPayment.insertUnscheduledLoanPayment(unscheduledPayment);
		Shift_Loan_Payment_Handler.saveLoanPaymentList( tempList );


		//Insert transaction records
		Shift_Transaction_Handler.createTransactionsForDischarge(loan, dischargePayment);

		//Loan is paid out in full close out all remaining payments and retire Loan
		Shift_AdjustLoanPaymentLines.dischargeRemainingPaymentSchedules(loan);
		Shift_Loan_Handler.retireLoan(loan);

	}
}