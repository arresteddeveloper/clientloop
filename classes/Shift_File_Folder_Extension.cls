public with sharing class Shift_File_Folder_Extension{

	public Loan__c theLoan {get;set;}

	private String AWSCredentialName = 'NAME OF KEY TO USE';
	public String bucketNameToCreate {get;set;}
	public String bucketNameToDelete {get;set;} 
	public String bucketSelected {get;set;}
	public String bucketToList {get;set;}
	public String bucketToUploadObject {get;set;}
	public S3.ListEntry[] bucketList {get;set;}    
	public String bucketNameForCreate {get;set;}
	public String bucketNameForDelete {get;set;}
	public S3.ListAllMyBucketsEntry[] allBucketList {get;set;}
	public String accessTypeSelected {get;set;}
	public String bucketNameToModifyAccess {get;set;}
	public String OwnerId {get;set;}
	public Boolean renderListBucketResults {get;set;}
	public String listBucketErrorMsg {get;set;}
	public String createBucketErrorMsg {get;set;}
	public String deleteBucketErrorMsg {get;set;}
	public String deleteObjectErrorMsg {get;set;}
	public String uploadObjectErrorMsg {get;set;}
	public String uploadObjectErrorMsg2 {get;set;}
	public String objectToDelete {get;set;}
	public String folderIdSelected {get;set;}
	public String bucketSelectToUploadForceDoc {get;set;}
	public String docToUploadName {get;set;}
	public String docToUploadId {get;set;}
	public Blob fileBlob {get;set;}
	public Integer fileSize {get;set;}
	public String fileName {get;set;}
	public Boolean showLoading {get; set;}
	public List<AWSElement> elementList {get; set;}

	public Shift_File_Folder_Extension(ApexPages.StandardController sc){
		theLoan = (Loan__c)sc.getRecord();
		bucketToList = Shift_Payment_Config__c.getOrgDefaults().AWS_Loan_Bucket__c;
		bucketToUploadObject = Shift_Payment_Config__c.getOrgDefaults().AWS_Loan_Bucket__c;
		accessTypeSelected = 'public-write';
	}

	public class AWSElement {

		public String Key {get;set;}
		
		public String FileName {
			get{
				return FileName;
			}
			set{
				FileName = value;
				FileName = FileName.substring(FileName.lastIndexOf('/') + 1);
			}
		}

		public String LastModified {
			get{
				return LastModified;
			}
			set{
				LastModified = value;
			}
		}

		public String Size {
			get{
				return Size;
			}
			set{
				Size = Shift_Utility.humanReadableByteCount(Long.valueOf(value));
			}
		}
	}

	public PageReference loadingStatus(){
		if (!bucketList.isEmpty()){
			showLoading = false;
		}
		return null;
	}
	/*
	   This method is called when the AWS_S3_Examples Visualforce page is loaded. It verifies that the AWS Keys can be found
	   in the AWSKeys__c custom object by the specified name, as set in the string variable AWSCredentialsName. 
	   
	   Any errors are added to the ApexPage and displayed in the Visualforce page. 
	*/
	public PageReference constructor(){
		try{
 
			AWSKeys credentials = new AWSKeys(AWSCredentialName);
			as3 = new S3.AmazonS3(credentials.key,credentials.secret);
			//as3.secret = credentials.secret;
			//as3.key = credentials.key;
			S3Key= credentials.key;
			renderListBucketResults = false;
			listBucketErrorMsg =null;
			createBucketErrorMsg=null;
			deleteBucketErrorMsg=null;
			showLoading = true;
		
		} catch(AWSKeys.AWSKeysException AWSEx){
			System.debug('Caught exception in AWS_S3_ExampleController: ' + AWSEx);
			ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.FATAL, AWSEx.getMessage());
			ApexPages.addMessage(errorMsg);
			//throw new AWSKeys.AWSKeysException(AWSEx);
			//ApexPages.addMessage(AWSEx);    
		}
		return listBucket();
	}

	public String S3Key {get;set;}
	
	public S3.AmazonS3 as3 { get; private set; } //This object represents an instance of the Amazon S3 toolkit and makes all the Web Service calls to AWS. 
  
  
	public S3.ListBucketResult listbucket {get;set;}
	public String selectedBucket {get;set;}
  
  
	//Method to return a string array for all the buckets in your AWS S3 account
	public String[] allBuckets { get {
	try{
	  
	Datetime now = Datetime.now();
	
	//This performs the Web Service call to Amazon S3 and retrieves all the Buckets in your AWS Account. 
	S3.ListAllMyBucketsResult allBuckets = as3.ListAllMyBuckets(as3.key,now,as3.signature('ListAllMyBuckets',now));
	
	//Store the Canonical User Id for your account
	OwnerId = allBuckets.Owner.Id;
	
	S3.ListAllMyBucketsList bucketList = allBuckets.Buckets;
	S3.ListAllMyBucketsEntry[] buckets = bucketList.Bucket;
	allBucketList = buckets;
	
	String[] bucketNames = new String[]{};
	
	
	//Loop through each bucket entry to get the bucket name and store in string array. 
	for(S3.ListAllMyBucketsEntry bucket: buckets){
		System.debug('Found bucket with name: ' + bucket.Name);
		bucketNames.add(bucket.name);
	}

	return bucketNames;

	}catch (System.NullPointerException e) {
		return null;
	}catch(Exception ex){
		//System.debug(ex);
		System.debug('caught exception in listallmybuckets');
		ApexPages.addMessages(ex);
		return null;  
	}

	}//end getter
	set;
   }
   
   /*
	 Method to list a bucket on AWS S3 
  
  */
  public PageReference listBucket(){
	try{   
		listBucketErrorMsg=  null;
		Datetime now = Datetime.now();
		Integer maxNumberToList = 25; //Set the number of Objects to return for a specific Bucket
		//String Prefix = null; //Limits the response to keys that begin with the indicated prefix. You can use prefixes to separate a bucket into different sets of keys in a way similar to how a file system uses folders. This is an optional argument.
		String Prefix = Shift_Payment_Config__c.getOrgDefaults().AWS_Loan_File_Prefix__c + theLoan.Id + '/';
		String Marker = null; //Indicates where in the bucket to begin listing. The list includes only keys that occur alphabetically after marker. This is convenient for pagination: To get the next page of results use the last key of the current page as the marker. The most keys you'd like to see in the response body. The server might return less than this number of keys, but will not return more. This is an optional argument.
		String Delimiter = null; //Causes keys that contain the same string between the prefix and the first occurrence of the delimiter to be rolled up into a single result element in the CommonPrefixes collection. These rolled-up keys are not returned elsewhere in the response. 
		System.debug('Going to execute S3 ListBucket service for bucket: ' + bucketToList + ', prefix: ' + Prefix );
	  
		//This performs the Web Service call to Amazon S3 and retrieves all the objects in the specified bucket
		S3.ListBucketResult objectsForBucket = as3.ListBucket(bucketToList, Prefix, Marker,maxNumberToList, Delimiter,as3.key,now,as3.signature('ListBucket',now),as3.secret);
		bucketList = objectsForBucket.Contents;
		bucketList.remove(0);
		createElementList();
		return null;
	}catch(System.CalloutException callout){
		System.debug('CALLOUT EXCEPTION: ' + callout);
		ApexPages.addMessages(callout);
		listBucketErrorMsg =   callout.getMessage();
		return null;   
	}catch(Exception ex){
		System.debug('EXCEPTION: ' + ex);
		listBucketErrorMsg =   ex.getMessage();
		ApexPages.addMessages(ex);
		return null;  
	}
  }

  private void createElementList(){

	elementList = new List<AWSElement>();
	for (S3.ListEntry l : bucketList){
		AWSElement temp = new AWSElement();
		temp.key = l.Key;
		temp.FileName = l.Key;
		temp.LastModified = l.LastModified.format();
		temp.Size = String.valueOf(l.Size);
		elementList.add (temp);
	}
  }
	/*
	 Method to create an object on AWS S3 
  }
  
  */
	public PageReference deleteObject(){
		try{
			deleteObjectErrorMsg= null;
			Datetime now = Datetime.now();        

			objectToDelete = ApexPages.currentPage().getParameters().get('keyToDelete');
			System.debug('about to delete S3 object with key: ' + objectToDelete);
			//This performs the Web Service call to Amazon S3 and create a new bucket.
			S3.Status deleteObjectReslt= as3.DeleteObject(bucketToList,objectToDelete,as3.key,now,as3.signature('DeleteObject',now), as3.secret);
			deleteObjectErrorMsg = 'Success';
				 
			System.debug('Successfully deleted a Bucket: ' + deleteObjectReslt.Description);
			return null;
		} catch(System.CalloutException callout){
			System.debug('CALLOUT EXCEPTION: ' + callout);
			ApexPages.addMessages(callout);
			deleteObjectErrorMsg = callout.getMessage();
			return null;  
		}
			catch(Exception ex){
			System.debug(ex);
			ApexPages.addMessages(ex);
			deleteObjectErrorMsg = ex.getMessage();
			return null;
		 }
	 
  }

	//This is used by the sample Visualforce page to display the list of all buckets created in your AWS account. 
	public List<SelectOption> getBucketNames() {
		List<SelectOption> options = new List<SelectOption>();
		
		String[] bckts = allBuckets;
		if(bckts!=null){
			for(String bucket : allBuckets){
				options.add(new SelectOption(bucket,bucket));  
			}
			return options;
		}
		else
		  return null;
	 }

	public pageReference redirectToS3Key() {
		//get the filename in urlencoded format
		String filename = EncodingUtil.urlEncode(ApexPages.currentPage().getParameters().get('filename'), 'UTF-8');
		//String bucket = EncodingUtil.urlEncode(ApexPages.currentPage().getParameters().get('bucket'), 'UTF-8');
		System.debug('redirectToS3Key filename: ' + filename);
		Datetime now = DateTime.now();
		Datetime expireson = now.AddSeconds(120);
		Long Lexpires = expireson.getTime()/1000;
		
		System.debug('key: ' + as3.key);
		System.debug('secret: ' + as3.secret);
		//String codedFilename=  EncodingUtil.urlEncode(filename,'UTF-8');
		//System.debug('codedFilename: '+codedFilename); 
		String stringtosign = 'GET\n\n\n'+Lexpires+'\n/'+bucketToList+'/'+filename;
		System.debug('redirectToS3Key stringstosign: ' + stringtosign);
		String signed = make_sig(stringtosign);
		System.debug('signed: ' + signed);
		String codedsigned = EncodingUtil.urlEncode(signed,'UTF-8');
		System.debug('codedsigned: ' + codedsigned);
		String url = 'http://'+bucketToList+'.s3.amazonaws.com/'+filename+'?AWSAccessKeyId='+as3.key+'&Expires='+Lexpires+'&Signature='+signed;
		System.debug('url: ' + url);
		PageReference newPage = new PageReference(url);
		System.debug('newPage url: ' + newPage.getUrl());
		return newPage;
	}

	//method that will sign
	private String make_sig(string canonicalBuffer) {        
		String macUrl ;
		String signingKey = EncodingUtil.base64Encode(Blob.valueOf(as3.secret));
		Blob mac = Crypto.generateMac('HMacSHA1', blob.valueof(canonicalBuffer),blob.valueof(as3.secret)); 
		macUrl = EncodingUtil.base64Encode(mac);                
		return macUrl;
	}

  /*
	  This method uploads a file from the filesystem and puts it in S3. 
	  It also supports setting the Access Control policy. 
  */
  public pageReference syncFilesystemDoc(){
	try{  
		Datetime now = Datetime.now();

		String docBody = EncodingUtil.base64Encode(fileBlob);

		//TODO - make sure doc.bodyLength is not greater than 100000 to avoid apex limits
		System.debug('body length: ' + fileSize);
		uploadObjectErrorMsg = 'Error';
		Boolean putObjResult = as3.PutObjectInline_ACL(bucketToUploadObject,fileName,null,docBody,fileSize,accessTypeSelected,as3.key,now,as3.signature('PutObjectInline',now),as3.secret, OwnerId);
		if(putObjResult==true){
			System.debug('putobjectinline successful');
			uploadObjectErrorMsg = 'Success';
		}
	}catch(System.CalloutException callout){
	System.debug('CALLOUT EXCEPTION: ' + callout);
	uploadObjectErrorMsg =   callout.getMessage();

	}catch(Exception ex){
		System.debug('EXCEPTION: ' + ex);
		uploadObjectErrorMsg =   ex.getMessage();
	}
	return null;  
  }
}