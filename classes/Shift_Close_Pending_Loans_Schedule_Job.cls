/* 
* Name          : Shift_Close_Pending_Loans_Schedule_Job
* Author        : Shift CRM
* Description   : Schedulable class to update Loans in stage 'Pending Loan Start', when Loan_Start_Date__c 
*                 less than or equal today, then Close the Loan.
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 01/27/2015         Desmond     1.0          Initial
*/

global class Shift_Close_Pending_Loans_Schedule_Job implements Schedulable{
    
    global List<Loan__c> loanList;

    global Shift_Close_Pending_Loans_Schedule_Job() {

        //Query for Loans where stage is 'Listed Loan' and Listing ends is before NOW
        loanList = [SELECT Id, RecordTypeId, Stage__c FROM Loan__c WHERE Stage__c = :Shift_Utility.PENDING_LOAN_START AND Loan_Start_Date__c <= TODAY ];
    }

    global void execute (SchedulableContext ctx){
        //Get Close Record Type for Loan__c
        RecordType rt = [SELECT Id, name, developerName FROM RecordType WHERE DeveloperName = :Shift_Utility.CLOSED_LOAN_DEVELOPER_NAME AND sObjectType = :Shift_Utility.LOAN_SOBJECT];

        //Set field values
        for (Loan__c l : loanList){

            //Set Fields
            l.RecordTypeId = rt.Id;
            l.Stage__c = Shift_Utility.CLOSED;

        }

        Database.update(loanList);
    }
}