/* 
* Name          : Shift_Loan_Part_Payment_Handler
* Author        : Shift CRM
* Description   : Handles functionality involving Loan Part Payments, inserts and updates
* Sharing Rules	: Without sharing modifier used to run as System
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 12/08/2014         Desmond     1.0          Initial
*/

public Without sharing class Shift_Loan_Part_Payment_Handler {

	/*
	* When a Loan's stage is set to Closed, this function is called to create the list of Loan Part Payments for the Loan to 
	* be inserted at the end of the trigger.
	*/
	public static Loan_Part_Payment__c[] createLoanPartPaymentOnLoanClose (Loan_Payment__c[] loanPaymentList){

		List<Id> loanIdList = new List<Id>();
		
		for (Loan_Payment__c lp : loanPaymentList){
			loanIdList.add(lp.loan__c);
		}

		//Query all Loan_Part__c
		Loan_Part__c[] LoanPartList = [SELECT Id, Loan__c FROM Loan_Part__c WHERE Loan__c IN :loanIdList];

		Loan_Part_Payment__c[] returnList = new List<Loan_Part_Payment__c>();

		//Iterate through list of Lenders
		for (Loan_Part__c lenders : LoanPartList){
			for (Loan_Payment__c loanPayment: loanPaymentList){
				if (lenders.Loan__c == loanPayment.Loan__c){
					returnList.add( new Loan_Part_Payment__c ( Loan_Part__c = lenders.Id, Master_Loan_Payment__c = loanPayment.Id) );
				}
			}
		}

		Database.insert(returnList, false);

		return returnList;
	}

	/*
	* Handler for deletion of Loan Payments
	*/
	public static void deleteLoanPartPayment (Loan_Payment__c[] loanPaymentList){

		//Query for the Loan Part Payments
		Map<Id,Loan_Payment__c> loanPaymentMap = new Map<Id,Loan_Payment__c>(loanPaymentList);
		Set<Id> loanPaymentIdSet = loanPaymentMap.keySet();
		Loan_Part_Payment__c[] deleteList = [SELECT Id FROM Loan_Part_Payment__c WHERE Master_Loan_Payment__c IN :loanPaymentIdSet];

		Database.delete(deleteList, false);

	}

}