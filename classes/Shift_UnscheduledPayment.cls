/* 
* Name          : Shift_UnscheduledPayment
* Author        : Shift CRM
* Description   : Class to handle unschedule payments to a loan
* Sharing Rules	: With sharing modifier used to run as User
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 12/10/2014         Desmond     1.0         Initial
* 
*/

public with sharing class Shift_UnscheduledPayment {

	/*
	* Unscheduled payments use the following function
	* - partial payment to principal is working fine for all three payment methods
	* - need to go over process for full payment as we have to handle additional fee calculations
	*/ 
	public static void makeUnscheduledPayment(Loan__c loan, Decimal amountPaid){

		//Query for the appropriate recordtype
		RecordType unschedulePaymentRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = :Shift_Utility.UNSCHEDULED_PAYMENT AND SobjectType = :Shift_Utility.LOAN_PAYMENT_SOBJECT];

		//Create the unscheduled Loan Payment for insertion
		Loan_Payment__c unscheduledPayment = new Loan_Payment__c (Loan__c = loan.id, 
									Principal_Paid__c = amountPaid,
									Fees_Paid__c = 0,
									Interest_Paid__c = 0,
									Interest_Scheduled__c = 0,
									Principal_Scheduled__c = 0,
									Fees_Owed__c = 0,
									Interest_Owed__c = 0,
									Principal_Owed__c = amountPaid,
									RecordTypeId = unschedulePaymentRecordType.Id,
									Payment_Date__c = Date.today()
									);


		//Insert Unscheduled Loan Payment
		insertUnscheduledLoanPayment(unscheduledPayment);

		//Insert transaction records
		Shift_Transaction_Handler.createTransactionsForEarlyPrincipalRepayment(loan, unscheduledPayment);

		loan = [SELECT Id, Total_Owed_by_Borrower__c, Amortization_Period__c, Loan_Amount__c, Outstanding_Principal__c, Amortization_Method__c, Interest_Rate__c, Loan_Term__c FROM loan__c WHERE Id = :loan.Id];

		Shift_AdjustLoanPaymentLines.adjustPaymentSchedule(loan);
		
		//If customer makes a partial payment to the outstanding principal
		//There are three amoritization methods to generate schedules from
/*		if (loan.Amortization_Method__c == Shift_Utility.STRAIGHT_LINE){
			Shift_AdjustLoanPaymentLines.straightLinePaymentSchedule(loan);
		} else if (loan.Amortization_Method__c == Shift_Utility.DECLINING_BALANCE){
			Shift_AdjustLoanPaymentLines.decliningBalancePaymentSchedule(loan);
		} else if (loan.Amortization_Method__c == Shift_Utility.INTEREST_ONLY){
			Shift_AdjustLoanPaymentLines.interestOnlyPaymentSchedule(loan);
		}*/

	}

	/*
	* Return list of Loan Payment records for a loan where payment due date is after today
	*/ 
/*	private static List<Loan_Payment__c> closeOutPrincipalAndInterestOnPaymentSchedules(Loan__c loan){
		//Pull all remaining Loan Payments schduled for after today and recordtype value
		RecordType scheduledPaymentRecordType =  [SELECT Id FROM RecordType WHERE DeveloperName = :Shift_Utility.SCHEDULED_PAYMENT AND SobjectType = :Shift_Utility.LOAN_PAYMENT_SOBJECT];
		List<Loan_Payment__c> paymentScheduleList = [SELECT Id, Principal_Owed__c, Interest_Owed__c, Payment_Number_Sequence__c FROM Loan_Payment__c WHERE Loan__c = :loan.Id AND RecordTypeId = :scheduledPaymentRecordType.Id AND Payment_Due_Date__c >= LAST_N_DAYS:7 ORDER By Payment_Number_Sequence__c ];	

		for (Loan_Payment__c currentLoanPayment: paymentScheduleList){

			currentLoanPayment.Principal_Owed__c = 0;

		}

		return paymentScheduleList;

	}*/

	/*
	* Insert unscheduled Loan Payment, while respecting errors and rolling them up to thier parent Loan record
	*/ 
	public static void insertUnscheduledLoanPayment (Loan_Payment__c loanPayment){
		
		if(loanPayment != null) {
      		Database.SaveResult result = Database.insert(loanPayment, false);	
        	Handle_Error(result, loanPayment);
        }

	}

	/*
	* Method for handling insertion/update errors
	*/
	@TestVisible
	private static void Handle_Error(Database.SaveResult result, Loan_Payment__c resultList){

		if (result.isSuccess()){

			System.debug('#### Update/Insert Operation Successful. Object ID: ' + result.getId());

		} else {

			for (Database.Error err : result.getErrors()){

		        System.debug('#### The following error has occurred.');
		        System.debug(err.getStatusCode() + ': ' + err.getMessage());
		        System.debug('#### Update/Insert Operation fields that affected this error: ' + err.getFields());
		        ApexPages.addmessage( new ApexPages.message(ApexPages.Severity.WARNING, err.getStatusCode() + ': ' + err.getMessage() ) );

			}

		}

	}

}