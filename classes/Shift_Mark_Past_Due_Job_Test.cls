@isTest
private class Shift_Mark_Past_Due_Job_Test {

	static testMethod void test_mark_past_due(){

		Shift_Utility_Test_Factory.setupOrg();
        Account[] borrowerList = setupBorrowers();

        //Setup Lender
        Account borrower = Shift_Utility_Test_Factory.createAccount();
        
        //Create a Loan
        Loan__c l = CreateLoan(borrower, Shift_Utility.DECLINING_BALANCE);
        insert l;
       
       	l.Stage__c = Shift_Utility.CLOSED;
       	update l;

        //Create Loan_Parts for Loan
        Loan_Part__c[] loanPartList = createLoanParts(l, borrowerList);
        insert loanPartList;

        Test.startTest();
        
        String CRON_EXP = '0 0 0 15 3 ? 2022';
        
        String jobId = System.schedule('MarkPastDue', CRON_EXP, new Shift_Mark_Past_Due_Job());       
        
        Test.stopTest();

	}

    private static Loan_Part__c[] createLoanParts(Loan__c theLoan, Account[] lenderList){
        Loan_Part__c[] returnList = new List<Loan_Part__c>();

        for (Account l : lenderList){
            returnList.add( new Loan_Part__c ( Amount_Lent__c = 1000,
                                               Lender__c = l.Id,
                                               Lender_Fee__c = 2,
                                               Loan__c = theLoan.Id,
                                               Status__c = 'Pledge'
                                             )
                      );
        }

        return returnList;
                      
    }

    static Account[]  setupBorrowers(){
        //Lenders Accounts
        Account[] accountList = createAccounts(5);
        insert accountList;

        //Give $1000 to Lenders and the Borrower via transactions
        Transaction__c[] moneyList = new List<Transaction__c>();
        RecordType transRT =  [SELECT Id FROM RecordType WHERE DeveloperName = :Shift_Utility.CREDIT_INTERNAL_DEVELOPER_NAME AND SobjectType = :Shift_Utility.TRANSACTION_SOBJECT];
        for (Account a : accountList){
            moneyList.add ( new Transaction__c ( Account__c = a.Id,
                                                      RecordTypeId = transRT.Id,
                                                      Method__c = 'Pre-Authorized Deposit', 
                                                      Status__c= 'Completed', 
                                                      Completed_On__c = System.now(), 
                                                      Type__c = 'Deposit', 
                                                      Credit_Amount__c = 1000)
                          );
        }
        insert moneyList;
        
        return accountList;
    }

    private static Loan__c createLoan(Account a, String method){       

    	//Today
    	Date todayDate = Date.today();
		//Last month
    	Date lastYear = todayDate.addMonths(-12);
        return new Loan__c( Name='shift test loan', 
                            Business_Name__c = a.Id,
                            Amortization_Method__c = method,
                            Payment_Frequency__c = 'Monthly',
                            Loop_Completion_Fee__c = 2,
                            Interest_Rate__c = 9,
                            Loan_Amount__c = 5000,
                            Loan_Term__c = 10,
                            Amortization_Period__c = 10,
                            Loan_Start_Date__c = lastYear,
                            Stage__c = Shift_Utility.PENDING_LOAN_START,
                            Listing_Begins__c = Date.today() - 1, 
                            Duration_of_Listing__c = 10
                            );
    }

    private static Account[] createAccounts(Integer size){
        Account[] returnList = new List<Account>();

        for (Integer i=0 ; i < size; i++){
            
            //returnList.add ( new Account (Name = 'shift test account ', RecordTypeId = temp.Id));
            returnList.add ( new Account (Name = 'shift test account ', status__c = 'Active', Verification__c = 'Verified'));
        }

        return returnList;
    }

    private static Loan__c[] createLoans(Account[] accountList){
        Loan__c[] returnList = new List<Loan__c>();

        for (Account a: accountList){
            returnList.add ( new Loan__c (name='shift test loan'));
        }

        return returnList;
    }
	
}